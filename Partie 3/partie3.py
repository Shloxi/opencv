import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

if not os.path.exists("data"):
    os.mkdir("data")

if not os.path.exists("../Images2022"):
    print("Folder Images2022 not found !")

# Question 1
# Import the image as gray
img = cv2.imread('../Images2022/vache.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Simple threshold
# If value above 130 it is shown as 255
(retVal, newImg) = cv2.threshold(img, 130, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
cv2.imwrite("data/simple_threshold.jpg", newImg)


# Import the image as gray 
img2 = cv2.imread('../Images2022/goldhill.jpg')
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)


# Adaptative vs Simple threshold
(retVal, newImg2_1) = cv2.threshold(img2, 130, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
newImg2_2 = cv2.adaptiveThreshold(img2, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 5)
plt.subplot(2,2,1),plt.imshow(img2,cmap = 'gray')
plt.title('Base'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(newImg2_1,cmap = 'gray')
plt.title('Simple'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(newImg2_2,cmap = 'gray')
plt.title('Adaptative'), plt.xticks([]), plt.yticks([])
plt.savefig("data/adaptative_vs_simple.jpg")


# Question 2
# Import the image as gray
img3 = cv2.imread('../Images2022/jeu1.jpg')
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
img4 = cv2.imread('../Images2022/jeu2.jpg')
img4 = cv2.cvtColor(img4, cv2.COLOR_BGR2GRAY)

newImg3 = cv2.bitwise_xor(img3, img4)
cv2.imwrite("data/differences.jpg", 255-newImg3)


# Question 3
# Import the image as gray
img5 = cv2.imread('../Images2022/cellules.png')
img5 = cv2.cvtColor(img5, cv2.COLOR_BGR2GRAY)

(retVal, newImg4) = cv2.threshold(img5,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

cv2.imwrite("data/binarization.png", newImg4)

res = cv2.connectedComponentsWithStats(newImg4, 4, cv2.CV_32S)

print("Nb cellules : " + str(res[0]-1))   


# Question 4
kernel = np.ones((6, 6), np.uint8)
newImg4 = cv2.erode(newImg4, kernel, iterations=1)

cv2.imwrite("data/erosion.png", newImg4)

res = cv2.connectedComponentsWithStats(newImg4, 4, cv2.CV_32S)

print("Nb cellules : " + str(res[0]-1))