import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

# Question 1
import numpy as np
import cv2

def dilate_processing(imgc):
    kernel = np.ones((3, 3), np.uint8)
    newImg = cv2.dilate(imgc, kernel, iterations=1)
    return newImg

def getHist(imgc):
    hist = cv2.calcHist([imgc], [0], None, [256],[0, 256])
    hist = cv2.normalize(hist, hist).flatten()
    return hist


# Import "data/jurassicworld.mp4" with OpenCV
cap = cv2.VideoCapture('../Images2022/jurassicworld.mp4')
oldHist = []
alreadyInchange = 0
# Infinite loop except for break
while (True):

    # Returns a tuple, ret is the error code value and frame the valid or not frame from cap
    ret, frame = cap.read()
    # If the frame is valid
    if ret == True:
        # We copy the current image and transform it into shades of grey
        img = frame.copy()
        
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        newHist = getHist(gray)
        if oldHist != []:
            metric_val = cv2.compareHist(newHist,oldHist,cv2.HISTCMP_BHATTACHARYYA)
            if metric_val > 0.15:
                if alreadyInchange != 1:
                    print("Changement de plan : " + str(metric_val))
                    alreadyInchange = 1
            else :
                alreadyInchange = 0
            oldHist = newHist
        else:
            oldHist = newHist

        # Display the original and grey frames
        cv2.imshow('MavideoAvant', frame)

    else:
        print('video ended')
        break

    if cv2.waitKey(60) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows() 