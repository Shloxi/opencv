import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

# Question 1
import numpy as np
import cv2

# Funcion that return the frame after processing
def subtract_processing(imgc, oldImg):
    if oldImg != [] :
        imgc = binarize_processing(imgc)
        newImg = cv2.subtract(imgc, oldImg)
        return newImg, imgc
    else:
        return imgc, imgc

def binarize_processing(imgc):
    _, newImg = cv2.threshold(imgc, 130, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return newImg

def dilate_processing(imgc):
    newImg = binarize_processing(imgc)
    kernel = np.ones((3, 3), np.uint8)
    newImg = cv2.dilate(newImg, kernel, iterations=1)
    return newImg

def erode_processing(imgc):
    newImg = binarize_processing(imgc)
    kernel = np.ones((3, 3), np.uint8)
    newImg = cv2.erode(newImg, kernel, iterations=1)
    return newImg

# Import "data/jurassicworld.mp4" with OpenCV
cap = cv2.VideoCapture('../Images2022/jurassicworld.mp4')
oldImg = []
# Infinite loop except for break
while (True):

    # Returns a tuple, ret is the error code value and frame the valid or not frame from cap
    ret, frame = cap.read()
    # If the frame is valid
    if ret == True:
        # We copy the current image and transform it into shades of grey
        img = frame.copy()
        
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Subtract_processing
        newImg, oldImg = subtract_processing(gray, oldImg)

        # Binarize processing
        #newImg = binarize_processing(gray)

        # Dilate processing
        #newImg = dilate_processing(gray)

        # Erode processing
        #newImg = erode_processing(gray)

        # Display the original and grey frames
        cv2.imshow('MavideoAvant', frame)
        cv2.imshow('MavideoApres', newImg)

    else:
        print('video ended')
        break

    if cv2.waitKey(60) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows() 