import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys
import csv
from os import walk

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

path = "Tagging"

# Definition des frontieres RGB pour chaque couleur
# Frontiere realises avec l'aide du site : https://www.rapidtables.com/web/color/RGB_Color.html#RGB-color-space
red_lb = [51,0,0]
red_ub = [255,51,51]
yellow_lb = [204,204,0]
yellow_ub = [255,255,153]
green_lb = [0,102,0]
green_ub = [102,255,102]
blue_lb = [0,0,102]
blue_ub = [153,255,255]
brown_lb = [51,25,0]
brown_ub = [153,76,0]
gray_lb = [41,41,41]
gray_ub = [192,192,192]
white_lb = [192,192,192]
white_ub = [255,255,255]
black_lb = [0,0,0]
black_ub = [40,40,40]
orange_lb = [204,102,0]
orange_ub = [255,153,51]

# Dictionnaire des frontieres pour chaque couleur
boundaries = {}
boundaries["red"] = (red_lb, red_ub)
boundaries["yellow"] = (yellow_lb, yellow_ub)
boundaries["green"] = (green_lb, green_ub)
boundaries["blue"] = (blue_lb, blue_ub)
boundaries["brown"] = (brown_lb, brown_ub)
boundaries["gray"] = (gray_lb, gray_ub)
boundaries["white"] = (white_lb, white_ub)
boundaries["black"] = (black_lb, black_ub)
boundaries["orange"] = (orange_lb, orange_ub)


if os.path.exists(path):
    # On parcours toutes les images du dossier
    filenames = next(walk(path), (None, None, []))[2]  # [] if no file
    for file in filenames:
        
        tag = ""
        img = cv2.imread(path + "/" + file,cv2.IMREAD_UNCHANGED)
        
        try:

            """
            On va definir le tag des couleurs avec l'aide de l'algorithme kmeans
            et des differentes frontieres que l'on a definit precedemment
            """
            # On restructure les donnees pour travailler dessus
            # Ici on on redefinit les donnees de sorte a avoir une liste du RGB de chaque pixel
            data = np.reshape(img, (-1,3))

            # Data doit etre de type float32 pour etre utilise par l'algorithme kmeans
            data = np.float32(data)

            # Critere de fin de l'algorithme kmeans | Iteration maximum = 10 | precision a atteindre = 1.0
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

            # Definit comment les centres initiaux de l'algorithme kmeans sont choisis. Ici on l'a definit de facon aleatoire
            flags = cv2.KMEANS_RANDOM_CENTERS

            # On applique l'algorithme kmeans de facon a avoir 2 clusters
            _, __, centers = cv2.kmeans(data,2,None,criteria,10,flags)

            # On a donc 1 centre pour chacun de nos 2 clusters qui represente nos 2 clusters
            # On va donc recuperer le RGB de ces 2 centres
            rgb1 = list(reversed(centers[0].astype(np.int32)))
            rgb2 = list(reversed(centers[1].astype(np.int32)))
    
            # On va definir les deux couleurs dominantes grace aux rgb obtenues precedemment et aux frontieres fixe manuellement
            color1 = ""
            color2 = ""

            for key, value in boundaries.items():
                # Si le premier RGB obtenu fait partie d'une de nos frontieres
                if (rgb1[0]>= value[0][0] and rgb1[0]<= value[1][0]) and (rgb1[1]>= value[0][1] and rgb1[1]<= value[1][1]) and (rgb1[2]>= value[0][2] and rgb1[2]<= value[1][2]):
                    # On ajoute le tag
                    color1 = key
                # Pareil pour le deuxieme RGB
                if (rgb2[0]>= value[0][0] and rgb2[0]<= value[1][0]) and (rgb2[1]>= value[0][1] and rgb2[1]<= value[1][1]) and (rgb2[2]>= value[0][2] and rgb2[2]<= value[1][2]):
                    color2 = key

            # Il est possible que notre RGB n'appartiennent a aucune de nos couleurs car les frontieres ne couvrent pas toutes les couleurs possibles
            # Ecriture des tags de couleur
            if color1 != "":
                tag += color1 + ","
            if color1 != color2 and color2 != "":
                if color1 != "":
                    tag += " " + color2 + ","
                else : 
                    tag += color2 + ","
            

            """
            On va definir le tag de complexite avec un algorithme de lissage, de binarisation simple et de contour Canny
            (Lissage pour eviter une trop grosse presence de contours, binarisation pour mieux reperer les contours et Canny qui affiche un nombre de contour juste)
            On va ensuite recuperer le nombre de regions dans notre image et si celle ci presente un trop petit nombre de regions
            l'image est considere comme solitaire sinon multiple selon deux seuils definis
            """
           
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # Lissage avec Filtre Gaussien
            gaussian = cv2.GaussianBlur(gray, (7, 7), 0)

            # Binarisation avec seuil adaptatif global
            _, binarized = cv2.threshold(gaussian, 130, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            
            # Detection de contour avec l'algorithme Canny
            t_lower = 50
            t_upper = 150
            imgc = cv2.Canny(binarized, t_lower, t_upper)
            
            # Calcul du nombre de regions sur l'image
            res = cv2.connectedComponentsWithStats(imgc, 4, cv2.CV_32S)

            # Definition de la complexite en fonction des seuils definis
            complexity = ""
            if res[0]-1 >= 1000 and complexity != "":
                complexity = "multiple"
            if res[0]-1 <= 50 and complexity != "":
                complexity = "solated"
            if tag != "" and complexity != "":
                tag += " " + complexity
            elif tag == "" and complexity != "":
                tag += complexity
        
            """
            Pour definir la luminosite de l'image on recupere juste la moyenne de tous les pixels de l'image transforme en HSV
            En fonction des seuils definis, l'image est sombre ou lumineuse
            """

            hsl  = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)

            # Calcul de la moyenne de l'image
            moyenne = np.mean(hsl)

            # Definition de la luminosite en fonction des seuils definis
            brightness = ""
            if moyenne <= 75:
                brightness = "dark,"
            if moyenne>150:
                brightness = "light,"
            if tag != "" and brightness != "":
                tag += " " + brightness
            elif tag == "" and brightness != "":
                tag += brightness


            """
            Pour definir le flou d'une image, on utilise la variance du filtre Laplacian
            Cela ne fonctionne pas vraiment
            """
            # Recupere la variance de l'image apres application du filtre Laplacien
            variance = cv2.Laplacian(gray, cv2.CV_64F).var()

            # Défintion du flou de l'image en fonction d'un seuil définit
            if variance <= 100:
                if tag != "":
                    tag += " blurry,"
                else:
                    tag += "blurry,"
            
            # On ecrit le tag
            fileTxt = file.split(".")
            with open(path + "/" + fileTxt[0] + ".txt", "w") as fic:
                fic.write(tag[:-1])
        except ValueError:
            print("Erreur lors de la lecture du fichier : " + file)   
else:
    print("Folder ImgTagging not found !")

