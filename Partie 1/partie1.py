import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

if not os.path.exists("data"):
    os.mkdir("data")

if not os.path.exists("../Images2022"):
    print("Folder Images2022 not found !")

# Import the image as gray
img = cv2.imread('../Images2022/monalisa.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(img, cmap='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(img.ravel(),256,[0,256])
plt.title('Histogram of monalisa'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_monalisa.png")
plt.close()

# Import the image as gray
img2 = cv2.imread('../Images2022/paysage.jpg')
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(img2, cmap='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(img2.ravel(),256,[0,256])
plt.title('Histogram of paysage'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_paysage.jpg")
plt.close()

eq_img2 = cv2.equalizeHist(img2)
np.hstack((img2,eq_img2)) #stacking images side-by-side

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(eq_img2, cmap = 'gray')
plt.title('Gray'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(eq_img2.ravel(),256,[0,256])
plt.title('Histogram of equalized paysage'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_eq_paysage.jpg")
plt.close()

# Import the image as gray
img3 = cv2.imread('../Images2022/soleil.png')
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(img3, cmap='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(img3.ravel(),256,[0,256])
plt.title('Histogram of soleil'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_soleil.png")
plt.close()

eq_img3 = cv2.equalizeHist(img3)
np.hstack((img3,eq_img3)) #stacking images side-by-side

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(eq_img3, cmap = 'gray')
plt.title('Gray'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(eq_img3.ravel(),256,[0,256])
plt.title('Histogram of equalized soleil'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_eq_soleil.png")
plt.close()

# Import the image as gray
img3 = cv2.imread('../Images2022/yellowstone.jpg')
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(img3, cmap='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(img3.ravel(),256,[0,256])
plt.title('Histogram of yellowstone'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_yellowstone.png")
plt.close()

eq_img3 = cv2.equalizeHist(img3)
np.hstack((img3,eq_img3)) #stacking images side-by-side

# Generate and save its histogram
plt.subplot(1,2,1),plt.imshow(eq_img3, cmap = 'gray')
plt.title('Gray'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2)
n, bins, patches= plt.hist(eq_img3.ravel(),256,[0,256])
plt.title('Histogram of equalized yellowstone'), plt.xticks([]), plt.yticks([])
plt.savefig("data/histogram_eq_yellowstone.png")
plt.close()