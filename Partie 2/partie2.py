import cv2
import numpy as np
from matplotlib import pyplot as plt

# Import libraries
import os
import sys

# Display the versions of the python installation and the opencv library but also the path of the working directory 
print("Python version")
print (sys.version)
print("Version info.")
print (sys.version_info)

print("OPENCV Version =", cv2.__version__)

if not os.path.exists("data"):
    os.mkdir("data")

if not os.path.exists("../Images2022"):
    print("Folder Images2022 not found !")

# Question 1
# Import the image as gray
img = cv2.imread('../Images2022/monalisa.png')
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Averaging Blur
averaging1 = cv2.blur(img, (5,5))
cv2.imwrite("data/Average_Blurring_5x5.png", averaging1)

# Averaging Blur
averaging2 = cv2.blur(img, (9,9))
cv2.imwrite("data/Average_Blurring_9x9.png", averaging2)

# Averaging Blur
averaging3 = cv2.blur(img, (15,15))
cv2.imwrite("data/Average_Blurring_15x15.png", averaging3)

# Averaging Blur
averaging3 = cv2.blur(img, (401,599))
cv2.imwrite("data/Average_Blurring_Full.png", averaging3)


# Question 2
# Import the image as gray
img2 = cv2.imread('../Images2022/delphin_dither.jpg')
img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

# Averaging Blur
averaging = cv2.blur(img2, (5,5))
cv2.imwrite("data/Average_Blurring.jpg", averaging)

# Gaussian Blur with 7x7 size
gaussian = cv2.GaussianBlur(img2, (7, 7), 0)
cv2.imwrite("data/Gaussian_Blurring.jpg", gaussian)
  
# Bilateral Blur
median = cv2.medianBlur(img2, 3)
cv2.imwrite("data/Bilateral_Blurring.jpg", median)

# Affiche l'image original, l'image avec le filtre Laplacien et l'image avec les filtres Sobel horizontaux et verticaux
plt.subplot(2,2,1),plt.imshow(img2,cmap = 'gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,2),plt.imshow(averaging,cmap = 'gray')
plt.title('Averaging'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,3),plt.imshow(gaussian,cmap = 'gray')
plt.title('Gaussian'), plt.xticks([]), plt.yticks([])
plt.subplot(2,2,4),plt.imshow(median,cmap = 'gray')
plt.title('Median'), plt.xticks([]), plt.yticks([])
plt.savefig("data/operation_on_delphin.jpg")

# Question 3
# Import the image as gray
img3 = cv2.imread('../Images2022/zebre.jpg')
img3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)

# Import the image as gray
img4 = cv2.imread('../Images2022/suzan.jpg')
img4 = cv2.cvtColor(img4, cv2.COLOR_BGR2GRAY)

sobely = cv2.Sobel(img3, cv2.CV_32F, 0, 1, ksize=5)
sobelx = cv2.Sobel(img4, cv2.CV_32F, 1, 0, ksize=5)

plt.subplot(1,2,1),plt.imshow(sobely,cmap = 'gray')
plt.title('Sobel Y k=5'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2),plt.imshow(sobelx,cmap = 'gray')
plt.title('Sobel X k=5'), plt.xticks([]), plt.yticks([])
plt.savefig("data/sobels.png")

# Question 4
def frame_processing(imgc):
    
    # Sobel X
    #imgc = cv2.Sobel(imgc, cv2.CV_32F, 1, 0, ksize=3)
    
    # Sobel Y
    #imgc = cv2.Sobel(imgc, cv2.CV_32F, 0, 1, ksize=3)
    
    #Laplacian
    #imgc = cv2.Laplacian(imgc, cv2.CV_32F)
    
    # Canny
    # Setting parameter values
    t_lower = 50  # Lower Threshold
    t_upper = 150  # Upper threshold
    
    imgc = cv2.Canny(imgc, t_lower, t_upper)
    
    return imgc

# Import "data/jurassicworld.mp4" with OpenCV
cap = cv2.VideoCapture('../Images2022/jurassicworld.mp4')

# Infinite loop except for break
while (True):

    # Returns a tuple, ret is the error code value and frame the valid or not frame from cap
    ret, frame = cap.read()

    # If the frame is valid
    if ret == True:
        # We copy the current image and transform it into shades of grey
        img = frame.copy()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # We process the image with the frame_processing function
        gray = frame_processing(gray)

        # Display the original and grey frames
        cv2.imshow('MavideoAvant', frame)
        cv2.imshow('MavideoApres', gray)

    else:
        print('video ended')
        break

    if cv2.waitKey(100) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows() 
  

